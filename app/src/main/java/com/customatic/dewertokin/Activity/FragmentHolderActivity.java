package com.customatic.dewertokin.Activity;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.customatic.dewertokin.Adapter.FragmentAdapter;
import com.customatic.dewertokin.Base.BaseActivity;
import com.customatic.dewertokin.Bluetooth.BluetoothLeService;
import com.customatic.dewertokin.Fragment.Fragment_LIST;
import com.customatic.dewertokin.Interactives.ActivityInteractive;
import com.customatic.dewertokin.Interactives.FragmentInteractive;
import com.customatic.dewertokin.Model.DeviceBluetooth;
import com.customatic.dewertokin.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class FragmentHolderActivity extends BaseActivity implements FragmentInteractive
        , ViewPager.OnPageChangeListener,ServiceConnection,BluetoothLeService.onServiceComminucator, RadioGroup.OnCheckedChangeListener,RadioGroup.OnHierarchyChangeListener {

    private ActivityInteractive activityInteractive;
    private BroadcastReceiver broadcastReceiver;
    private BluetoothLeService bluetoothLeService;
    private BluetoothManager bluetoothManager;
    private Intent serviceIntent;
    private ArrayList<DeviceBluetooth> scanResultArrayList;
    private ArrayList<DeviceBluetooth> connectedBluetooth;
    private ArrayList<BluetoothGatt> bluetoothGatts;
    private int disconnectionCount = -1;
    private boolean hasConnected = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holder_fragment);
        bluetoothGatts = new ArrayList<>();

    }

    @Override
    protected void onStart() {
        super.onStart();
        ViewPager viewPager = this.findViewById(R.id.pager);
        FragmentAdapter fragmentAdapter = new FragmentAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
                ,this);
        viewPager.setAdapter(fragmentAdapter);
        viewPager.addOnPageChangeListener(this);
        scanResultArrayList = new ArrayList<>();
        connectedBluetooth = new ArrayList<>();
        this.broadcastReceiver = startAndInitReciever();

       /* TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_device_hub_black_24dp);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_massage_24dp);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_lift);
*/
        serviceIntent = new Intent(getApplicationContext(),BluetoothLeService.class);
         bindService(serviceIntent,this, Context.BIND_AUTO_CREATE);
        startService(serviceIntent);


        RadioGroup radioGroup = findViewById(R.id.radio_group);
        radioGroup.removeAllViews();
        radioGroup.setOnCheckedChangeListener(this);
        radioGroup.setOnHierarchyChangeListener(this);

        View radio_view = findViewById(R.id.radio_holder);

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
if (activityInteractive != null){
    activityInteractive.onActivityContinuedCallback(bluetoothGatts,true);
}
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void splash_end() {

    }

    @Override
    public void onFragmentCallback(Object data) {
     if (data instanceof ActivityInteractive){
         activityInteractive = (ActivityInteractive) data;
     }
     if(data instanceof Fragment_LIST){
         if(scanResultArrayList != null && activityInteractive != null && ((Fragment_LIST) data).isLoaded()){
             scan(true);
         }
     }
     if(data instanceof ScanResult){
        hasConnected = bluetoothLeService.connect(((ScanResult) data).getDevice().getAddress());
     }
     if (data instanceof byte[]){
         if(hasConnected){
             View radio_view = findViewById(R.id.radio_holder);
             radio_view.setVisibility(View.VISIBLE);
             RadioGroup radioGroup = findViewById(R.id.radio_group);
             if(radioGroup.getCheckedRadioButtonId() != -1 && checkIndex != -1) {
                 bluetoothLeService.write((byte[]) data,checkIndex);
             }
             else {
                 Toast.makeText(getApplicationContext(), "Please Select a Controller Box", Toast.LENGTH_SHORT).show();
             }
             }
         else {
             Toast.makeText(getApplicationContext(), "Select a device", Toast.LENGTH_SHORT).show();
             ViewPager viewPager = this.findViewById(R.id.pager);
             viewPager.setCurrentItem(2,true);
         }
     }
     }

    @Override
    public boolean onFragmentContinuedCallback(Object data, boolean start) {
    if (start){
        if(activityInteractive != null){
            activityInteractive.onActivityContinuedCallback(bluetoothGatts,start);
        }
    }
        if(data instanceof ScanResult){
            if(!start) {
              bluetoothLeService.disconnect(((ScanResult) data).getDevice().getAddress());
            }
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(this.broadcastReceiver != null) {
            closeAndRemoveReciever(this.broadcastReceiver);
        }
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
       BluetoothLeService.LocalBinder localBinder = (BluetoothLeService.LocalBinder) iBinder;
       bluetoothLeService = localBinder.getService();
       bluetoothLeService.initialize();
       bluetoothLeService.setOnServiceComminucator(this);
       bluetoothManager = bluetoothLeService.getBluetoothManager();
       bluetoothLeService.setBluetoothAdapter(this.getBluetoothAdapter());

    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {

    }

    @Override
    public void onBackPressed() {

    }

    protected void scan(final boolean enable) {

        final BluetoothLeScanner bluetoothLeScanner = getBluetoothAdapter().getBluetoothLeScanner();
        if (enable) {
            // Stops scanning after a pre-defined sx    can period.
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    bluetoothLeScanner.stopScan(scanCallback);
                    if(activityInteractive != null) {
                        activityInteractive.onActivityCallback(scanResultArrayList);
                    }
                }
            }, 5000);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                ScanSettings settings = new ScanSettings.Builder()
                        .setLegacy(false)
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                        .build();
                List<ScanFilter> filters = new ArrayList<>();
                filters.add(new ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString("62741523-52f9-8864-b1ab-3b3a8d65950b")).build());
                bluetoothLeScanner.startScan(filters,settings,scanCallback);
            }else{
            bluetoothLeScanner.startScan(scanCallback);
            }

        } else {
            bluetoothLeScanner.stopScan(scanCallback);
        }
    }
    // Device scan callback.
    private ArrayList<String> address = new ArrayList<>();
    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            if(!address.contains(result.getDevice().getAddress()) && result.getDevice().getName() != null){
                address.add(result.getDevice().getAddress());
                if(getBluetoothAdapter().getBondedDevices().contains(result.getDevice())) {
                    scanResultArrayList.add(new DeviceBluetooth(true, result));

                }
                else{
                    scanResultArrayList.add(new DeviceBluetooth(false, result));
                }
            }

        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);

        }
    };

    @Override
    public void onCall(Object data,boolean connected) {
        if(activityInteractive != null){
            if(data instanceof BluetoothGatt) {
                activityInteractive.onActivityContinuedCallback( ((BluetoothGatt) data).getDevice(),connected);
                if(connected && !bluetoothGatts.contains((BluetoothGatt) data)){
                    bluetoothGatts.add((BluetoothGatt) data);
                }else if(bluetoothGatts.contains((BluetoothGatt) data)){
                    bluetoothGatts.remove((BluetoothGatt)data);
                }
                addRadioGroup();
                activityInteractive.onActivityContinuedCallback(bluetoothGatts,connected);

            }
        }
    }

    @Override
    public void newConnection(boolean isConnected) {
        hasConnected = isConnected;
    }

    private void addRadioGroup(){
        final int[][] states = new int[][] {
                new int[] { android.R.attr.state_enabled}, // enabled
        };

        final int[] colors = new int[] {
                Color.WHITE,
        };

        final RadioGroup radioGroup = findViewById(R.id.radio_group);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                radioGroup.removeAllViews();
            }
        });
        checkIndex = -1;
        int count = 0;
        for (final BluetoothGatt bluetoothGatt : bluetoothGatts){
            final int finalCount = count;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final RadioButton radioButton = new RadioButton(getApplicationContext());
                    radioButton.setText(bluetoothGatt.getDevice().getName());
                    radioButton.setTextColor(Color.WHITE);
                    radioButton.setId(finalCount);
                    radioButton.setButtonTintList(new ColorStateList(states,colors));
                    radioGroup.addView(radioButton);
                    radioGroup.setPadding(6,6,6,6);
                }
            });
            count++;
        }

    }

    private int checkIndex = -1;
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        checkIndex = i ;
    }

    @Override
    public void onChildViewAdded(View view, View view1) {
        Log.d("TAG", "onChildViewAdded: ");
    }

    @Override
    public void onChildViewRemoved(View view, View view1) {
        Log.d("TAG", "onChildViewRemoved: ");
        final RadioGroup radioGroup = findViewById(R.id.radio_group);
        radioGroup.clearCheck();
    }
}
