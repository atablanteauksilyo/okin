package com.customatic.dewertokin.Adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.customatic.dewertokin.Fragment.Fragment_LIFT;
import com.customatic.dewertokin.Fragment.Fragment_LIST;
import com.customatic.dewertokin.Fragment.Fragment_MASSAGE;
import com.customatic.dewertokin.Interactives.FragmentInteractive;

public class FragmentAdapter extends FragmentStatePagerAdapter {
    private FragmentInteractive interactive;
    public FragmentAdapter(@NonNull FragmentManager fm, int behavior, FragmentInteractive interactive) {
        super(fm, behavior);
        this.interactive = interactive;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new Fragment_LIFT(interactive);
            case 1: return new Fragment_MASSAGE(interactive);
            case 2: return new Fragment_LIST(interactive);
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0 : return "Lift";
            case 1 : return "Massage";
            case 2 : return "Devices";
        }
        return  null;
    }
}
