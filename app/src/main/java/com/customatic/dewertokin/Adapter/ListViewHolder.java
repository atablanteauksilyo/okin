package com.customatic.dewertokin.Adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.customatic.dewertokin.R;

public class ListViewHolder extends RecyclerView.ViewHolder
{

    TextView textView;
    TextView textView2;
    CardView cardView;
    ImageView imageView;
    public ListViewHolder(@NonNull View itemView) {
        super(itemView);
        textView2 = itemView.findViewById(R.id.connect_data);
        cardView = itemView.findViewById(R.id.base_holder);
        textView = itemView.findViewById(R.id.address_data);
        imageView = itemView.findViewById(R.id.disconnect);

    }
}
