package com.customatic.dewertokin.Adapter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.customatic.dewertokin.Interactives.ItemInteractive;
import com.customatic.dewertokin.Model.DeviceBluetooth;
import com.customatic.dewertokin.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<ListViewHolder>  {
    private View view;
    private ArrayList<DeviceBluetooth> scanResults;
    private ItemInteractive itemInteractive;
    private Context context;
    public RecyclerAdapter(ItemInteractive itemInteractive, ArrayList<DeviceBluetooth> results, Context context){
        this.itemInteractive = itemInteractive;
        this.scanResults = results;
        this.context = context;
    }


    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_dom,parent,false);
        ListViewHolder listViewHolder = new ListViewHolder(view);
        return listViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, final int position) {

        holder.textView.setText(this.scanResults.get(position).getScanResult().getDevice().getAddress());

        if(scanResults.get(position).getScanResult().getDevice().getName() != null){
            holder.textView.setText(this.scanResults.get(position).getScanResult().getDevice().getName());

        }
        if(scanResults.get(position).isConnected()){
            holder.textView2.setText("Connected");
            holder.textView2.setTextColor(Color.GREEN);
            holder.imageView.setVisibility(View.VISIBLE);
            itemInteractive.onItemCallback(scanResults.get(position).getScanResult(),true);
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemInteractive.onItemCallback(scanResults.get(position).getScanResult(),false);
                }
            });
        }
        else{
            holder.textView2.setText("Not Connected");
            holder.textView2.setTextColor(context.getColor(R.color.colorPrimary));
            holder.imageView.setVisibility(View.GONE);
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemInteractive.onItemCallback(scanResults.get(position).getScanResult(),true);
                }
            });
        }
    }



    @Override
    public int getItemCount() {
        return this.scanResults.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

}
