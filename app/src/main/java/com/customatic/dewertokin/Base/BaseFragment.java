package com.customatic.dewertokin.Base;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.customatic.dewertokin.Interactives.ActivityInteractive;
import com.customatic.dewertokin.Interactives.FragmentInteractive;
import com.customatic.dewertokin.R;

public abstract class BaseFragment extends Fragment implements ActivityInteractive{
    protected FragmentInteractive interactive;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        interactive.onFragmentCallback(this);
    }

    @Override
    public boolean onActivityContinuedCallback(Object data, boolean start) {
        return false;
    }

    @Override
    public void onActivityCallback(Object data) {

    }

    protected AlertDialog showAlertDialog(String title , Activity activity ,int layoutRes){

        View  v = activity.getLayoutInflater().inflate(layoutRes,null);

        final AlertDialog alertDialog = new AlertDialog.Builder(activity).setView(v)
                .setCancelable(false).create();
        alertDialog.show();


        return alertDialog;
    }
}
