package com.customatic.dewertokin.Bluetooth;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.aware.Characteristics;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.customatic.dewertokin.Command.CommandList;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.UUID;


public final class BluetoothLeService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
    // A service that interacts with the BLE device via the Android BLE API.
        private final static String TAG = "DATA BLE";

        private BluetoothManager bluetoothManager;
        private BluetoothAdapter bluetoothAdapter;
        private int connectionState = STATE_DISCONNECTED;
        private BluetoothGatt[] bluetoothGatts = new BluetoothGatt[3];
        private onServiceComminucator onServiceComminucator;
        private static final int STATE_DISCONNECTED = 0;
        private static final int STATE_CONNECTING = 1;
        private static final int STATE_CONNECTED = 2;
    public final static String ACTION_GATT_CONNECTED =
            "com.nordicsemi.nrfUART.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.nordicsemi.nrfUART.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.nordicsemi.nrfUART.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.nordicsemi.nrfUART.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.nordicsemi.nrfUART.EXTRA_DATA";
    public final static String DEVICE_DOES_NOT_SUPPORT_UART =
            "com.nordicsemi.nrfUART.DEVICE_DOES_NOT_SUPPORT_UART";
    private static final UUID MY_UUID_INSECURE =
            UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");

    public static final UUID TX_POWER_UUID = UUID.fromString("00001800-0000-1000-8000-00805f9b34fb");
    public static final UUID FIRMWARE_REVISON_UUID = UUID.fromString("00001720-0000-1000-8000-00805f9b34fb");

    public static final UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final UUID RX_SERVICE_UUID = UUID.fromString("62741523-52f9-8864-b1ab-3b3a8d65950b");

    public static final UUID WRITE_SERVICE_UUID = UUID.fromString("62741523-52F9-8864-B1AB-3B3A8D65950B");
    public static final UUID READ_SERVICE_UUID = UUID.fromString("62741523-52F9-8864-B1AB-3B3A8D65950B");

    public static final UUID WRITE_CHARACTERISTIC_UUID = UUID.fromString("62741525-52F9-8864-B1AB-3B3A8D65950B");
    public static final UUID READ_CHARACTERISTIC_UUID = UUID.fromString("62741625-52F9-8864-B1AB-3B3A8D65950B");

    public static final UUID RX_CHAR_UUID = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
    public static final UUID TX_CHAR_UUID = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");

        // Various callback methods defined by the BLE API.
        public final BluetoothGattCallback gattCallback =
                new BluetoothGattCallback() {

                    @Override
                    public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
                        super.onMtuChanged(gatt, mtu, status);
                        Log.d(TAG, "onMtuChanged: ");
                    }
                    

                    @Override
                    public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                                        int newState) {
                        String intentAction;
                        if (newState == BluetoothProfile.STATE_CONNECTED) {
                            intentAction = ACTION_GATT_CONNECTED;
                            connectionState = STATE_CONNECTED;
                            broadcastUpdate(intentAction);
                            Log.d(TAG, "onConnectionStateChange: ");
                            gatt.discoverServices();

                        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                            intentAction = ACTION_GATT_DISCONNECTED;
                            connectionState = STATE_DISCONNECTED;
                            Log.d(TAG, "onConnectionStateChange: disconnected");
                            if(onServiceComminucator != null) {
                                onServiceComminucator.onCall(gatt,false);
                                onServiceComminucator.newConnection(false);
                            }
                            int counter = 0;
                            while (counter < bluetoothGatts.length){
                                if(bluetoothGatts[counter].getDevice().getAddress().equals(gatt.getDevice().getAddress())){
                                    bluetoothGatts[counter]=null;
                                    break;
                                }
                                counter++;
                            }

                            broadcastUpdate(intentAction);
                        }
                    }

                    @Override
                    // New services discovered
                    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                        if (status == BluetoothGatt.GATT_SUCCESS) {
                            broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
                            int count= 0 ;

                            while (count < gatt.getServices().size()){
                               // Log.d(TAG, "onServicesDiscovered: " + gatt.getServices().get(count).getUuid());
                                count++;
                            }if(onServiceComminucator != null) {
                                onServiceComminucator.onCall(gatt,true);
                                onServiceComminucator.newConnection(true);
                            }
                            setCharacteristicReciever(gatt );
                            //getFirmwareVersion();
                            write(CommandList.Bluetooth.CBOX_AUTHENTICATION, 0);

                        }

                    }

                    @Override
                    // Result of a characteristic read operation
                    public void onCharacteristicRead(BluetoothGatt gatt,
                                                     BluetoothGattCharacteristic characteristic,
                                                     int status) {
                      if (characteristic.getUuid().equals(UUID.fromString("00002a00-0000-1000-8000-00805f9b34fb"))){
                          gatt.getDevice().createBond();
                          Log.d(TAG, "onCharacteristicRead: ");
                      }
                        if (status == BluetoothGatt.GATT_SUCCESS) {
                            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
                        }
                    }

                    @Override
                    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                        super.onCharacteristicChanged(gatt, characteristic);
                        broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
                        Log.d(TAG, "onCharacteristicChanged: ");
                    }

                    @Override
                    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                        super.onCharacteristicWrite(gatt, characteristic, status);
                        if(status == BluetoothGatt.GATT_SUCCESS){
                            Log.d(TAG, "onCharacteristicWrite: ");
                        }
                    }

                    @Override
                    public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                        super.onDescriptorWrite(gatt, descriptor, status);
                        Log.d(TAG, "onDescriptorWrite: ");
                    }

                    @Override
                    public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                        super.onDescriptorRead(gatt, descriptor, status);
                        Log.d(TAG, "onDescriptorRead: ");
                    }
                };


    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void setOnServiceComminucator(onServiceComminucator onServiceComminucator){
        this.onServiceComminucator = onServiceComminucator;
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);


        // This is special handling for the Heart Rate Measurement profile. Data
        // parsing is carried out as per profile specifications.
        if (TX_CHAR_UUID.equals(characteristic.getUuid())) {

            // Log.d(TAG, String.format("Received TX: %d",characteristic.getValue() ));
            intent.putExtra(EXTRA_DATA, characteristic.getValue());
        } else if(FIRMWARE_REVISON_UUID.equals(characteristic.getUuid())) {
            // Firmware UUID
            intent.putExtra(EXTRA_DATA, characteristic.getValue());
        } else {

        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
    public static String getGuidFromByteArray(byte[] bytes) {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
            long high = bb.getLong();
        long low = bb.getLong();
        UUID uuid = new UUID(high, low);
        return uuid.toString();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public class LocalBinder extends Binder {
        public BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }
    private final IBinder mBinder = new LocalBinder();


    public void setBluetoothAdapter(BluetoothAdapter bluetoothAdapter) {
        this.bluetoothAdapter = bluetoothAdapter;
    }

    public boolean connect(String address){
        int counter = 0;
        while (counter < bluetoothGatts.length){

            if(bluetoothGatts[counter] == null){
                final BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
                bluetoothGatts[counter] = device.connectGatt(getApplicationContext(),false,gattCallback);
                Log.d(TAG, "connect: " + counter);
                return true;
            }
            else if (bluetoothGatts[counter].getDevice().getAddress().equals(address)){
                Log.d(TAG, "connect_base: " + counter);
                return true;
            }
            counter++;
        }

     return false; }

     public void setCharacteristicReciever(BluetoothGatt gatt){
       BluetoothGattService bluetoothGattService = gatt.getService(WRITE_SERVICE_UUID);
       if(bluetoothGattService != null){

           BluetoothGattCharacteristic bluetoothGattCharacteristic = bluetoothGattService.getCharacteristic(WRITE_CHARACTERISTIC_UUID);
           gatt.setCharacteristicNotification(bluetoothGattCharacteristic,true);
           int count = 0 ;
           while(count < bluetoothGattCharacteristic.getDescriptors().size()){
               bluetoothGattCharacteristic.getDescriptors().get(count).setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
               gatt.writeDescriptor(bluetoothGattCharacteristic.getDescriptors().get(count));
               count++;
           }

       }
     }

 public void close_gatt(int index){
   if(bluetoothGatts[index] != null){
       bluetoothGatts[index].close();
   }
 }

    public BluetoothManager getBluetoothManager() {
        return bluetoothManager;
    }

    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (bluetoothManager == null) {
            bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);

            if (bluetoothManager== null) {
                return false;
            }
        }

        bluetoothAdapter = bluetoothManager.getAdapter();
        if (bluetoothAdapter== null) {
            return false;
        }

        return true;
    }

    public void write(byte[] value, int gattIndex) { //gatIndex = 0 for gatt1, 1 for gatt2 and 2 for tandem gatt
        if (bluetoothGatts.length > gattIndex) {
            Log.d(TAG, "write: " + bluetoothGatts[gattIndex].getDevice().getName() +  " "+ gattIndex);
            writeRXCharacteristic(value, bluetoothGatts[gattIndex]);
        }
    }
    private void writeRXCharacteristic(final byte[] value, final BluetoothGatt gatt) {
        try {
            if (gatt != null) {
                if(value == CommandList.Bluetooth.CBOX_AUTHENTICATION){
                    BluetoothGattCharacteristic bluetoothGattCharacteristic =
                            gatt.getService(TX_POWER_UUID).getCharacteristic(UUID.fromString("00002a00-0000-1000-8000-00805f9b34fb"));
                    gatt.readCharacteristic(bluetoothGattCharacteristic);
                    gatt.getDevice().createBond();
                    return;
                }

                BluetoothGattService RxService = gatt.getService(WRITE_SERVICE_UUID);
                if (RxService == null) {
                    broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
                    return;
                }
                BluetoothGattCharacteristic RxChar = RxService.getCharacteristic(WRITE_CHARACTERISTIC_UUID);
                if (RxChar == null) {
                    broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
                    return;
                }
                 RxChar.setValue(value);
                 gatt.writeCharacteristic(RxChar);
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean disconnect(String address) {
        int count = 0;
        while (count < bluetoothGatts.length){
            Log.d(TAG, "disconnect: ");
            if(bluetoothGatts[count] != null && bluetoothGatts[count].getDevice().getAddress().equals(address)){
                bluetoothGatts[count].disconnect();
                unpairDevice(bluetoothGatts[count].getDevice());
                bluetoothGatts[count] = null;
                return false;
            }
            count++;
        }
        return true;
    }

    private void unpairDevice(BluetoothDevice device) {
        try {
            Method m = device.getClass()
                    .getMethod("removeBond", (Class[]) null);
            m.invoke(device, (Object[]) null);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }
    public void enableTXNotification()
    {
    	/*
    	if (mBluetoothGatt == null) {
    		showMessage("mBluetoothGatt null" + mBluetoothGatt);
    		broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
    		return;
    	}
    		*/
        BluetoothGattService RxService = bluetoothGatts[0].getService(RX_SERVICE_UUID);
        if (RxService == null) {
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);

            return;
        }
        BluetoothGattCharacteristic TxChar = RxService.getCharacteristic(TX_CHAR_UUID);
        if (TxChar == null) {
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
            return;
        }
        bluetoothGatts[0].setCharacteristicNotification(TxChar,true);

        BluetoothGattDescriptor descriptor = TxChar.getDescriptor(CCCD);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        bluetoothGatts[0].writeDescriptor(descriptor);

    }

    public void getFirmwareVersion () {
        BluetoothGattService RxService = bluetoothGatts[0].getService(RX_SERVICE_UUID);
        if (RxService == null) {
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
            Log.d(TAG, "getFirmwareVersion: 1");
            return;
        }

        BluetoothGattCharacteristic TxFirmwareChar = RxService.getCharacteristic(FIRMWARE_REVISON_UUID);
        if (TxFirmwareChar == null) {
            Log.d(TAG, "getFirmwareVersion: 2");
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
            return;
        }

        readCharacteristic(TxFirmwareChar);
    }
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (bluetoothAdapter== null || bluetoothGatts[0] == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        int counter = 0;
        while(counter < bluetoothGatts.length){
            if(bluetoothGatts[counter] != null){
                bluetoothGatts[counter].readCharacteristic(characteristic);
            }
            counter++;
        }
    }

    public interface onServiceComminucator <T>{
       void onCall(T data ,boolean connected);
       void newConnection(boolean isConnected);
    }

}

