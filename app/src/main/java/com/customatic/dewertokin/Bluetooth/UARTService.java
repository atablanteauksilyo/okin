package com.customatic.dewertokin.Bluetooth;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.List;
import java.util.UUID;

public final class UARTService extends Service {
    private final static String TAG = UARTService.class.getSimpleName();

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;

    private String[] mBluetoothDeviceAddress = new String[3];
    private BluetoothGatt[] mBluetoothGatt = new BluetoothGatt[3];
    private int mConnectionState = STATE_DISCONNECTED;
    private int newConnectionIndex = 0;

    private static final int STATE_DISCONNECTED = -1;
    private static final int STATE_CONNECTING = 0;
    private static final int STATE_CONNECTED = 1;

    public final static String ACTION_GATT_CONNECTED =
            "com.nordicsemi.nrfUART.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.nordicsemi.nrfUART.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.nordicsemi.nrfUART.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.nordicsemi.nrfUART.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.nordicsemi.nrfUART.EXTRA_DATA";
    public final static String FIRMWARE_EXTRA_DATA =
            "com.nordicsemi.nrfUART.FIRMWARE_EXTRA_DATA";
    public final static String DEVICE_DOES_NOT_SUPPORT_UART =
            "com.nordicsemi.nrfUART.DEVICE_DOES_NOT_SUPPORT_UART";

    public  final static String ACTION_SERVICE_OK =
            "com.example.senoo_junya.sampleremote.action.SERVICEOK";

    public static final UUID TX_POWER_UUID = UUID.fromString("00001804-0000-1000-8000-00805f9b34fb");
    public static final UUID TX_POWER_LEVEL_UUID = UUID.fromString("00002a07-0000-1000-8000-00805f9b34fb");
    public static final UUID FIRMWARE_REVISON_UUID = UUID.fromString("00002a26-0000-1000-8000-00805f9b34fb");
    public static final UUID DIS_UUID = UUID.fromString("0000180a-0000-1000-8000-00805f9b34fb");

    public static final UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final UUID RX_SERVICE_UUID = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
    public static final UUID RX_CHAR_UUID = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
    public static final UUID TX_CHAR_UUID = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");


    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = ACTION_GATT_CONNECTED;
                mConnectionState = STATE_CONNECTED;
                broadcastUpdate(intentAction);
                Log.i(TAG, "Connected to GATT server.");
                // Attempts to discover services after successful connection.
                Log.i(TAG, "Attempting to start service discovery:" +
                        mBluetoothGatt[newConnectionIndex].discoverServices());

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;
                Log.i(TAG, "Disconnected from GATT server.");
                broadcastUpdate(intentAction);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.w(TAG, "mBluetoothGatt = " + mBluetoothGatt );

                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            Log.d(TAG, "onCharacteristicRead!!!");
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            Log.d(TAG, "onCharacteristicChanged!!!");
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
        }

        @Override
        public void onDescriptorWrite (BluetoothGatt gatt,
                                       BluetoothGattDescriptor desc,
                                       int status) { // writeDescriptor() 結果
            super.onDescriptorWrite(gatt, desc, status);
            Log.d(TAG, "onDescriptorWrite: sts=" + status);
            broadcastUpdate(ACTION_SERVICE_OK);
        }
    };

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        // This is handling for the notification on TX Character of NUS service
        if (TX_CHAR_UUID.equals(characteristic.getUuid())) {

            // Log.d(TAG, String.format("Received TX: %d",characteristic.getValue() ));
            intent.putExtra(EXTRA_DATA, characteristic.getValue());
        } else if(FIRMWARE_REVISON_UUID.equals(characteristic.getUuid())) {
            // Firmware UUID
            intent.putExtra(FIRMWARE_EXTRA_DATA, characteristic.getValue());
        } else {

        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public class LocalBinder extends Binder {
        public UARTService getService() {
            return UARTService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }


    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }

        if(mBluetoothGatt[0] == null || mBluetoothDeviceAddress[0].equals(device.getAddress())) {
            mBluetoothGatt[0] = device.connectGatt(this, false, mGattCallback);
            System.out.println("Trying to create a new connection.");
            mBluetoothDeviceAddress[0] = address;
            mConnectionState = STATE_CONNECTING;
            newConnectionIndex = 0;
        } else {
            mBluetoothGatt[1] = device.connectGatt(this, false, mGattCallback);
            System.out.println("Trying to create a new connection.");
            mBluetoothDeviceAddress[1] = address;
            mConnectionState = STATE_CONNECTING;
            newConnectionIndex = 1;
        }

        if(isInTandemMode()) {
        }
        return true;
    }



    public void reconnect(String[] addresses) {
        List<BluetoothDevice> list = mBluetoothManager.getConnectedDevices(BluetoothProfile.GATT);
        for (int i = 0; i < 2; i++) {// String a : addresses) {
            String a = addresses[i];
            if(a != null) {
                if(!list.contains(a)) {//BLUETOOTHGATT CONNECT AGAIN LIKE RECONNECTION CHECK IF TRUE.
                    if (mBluetoothGatt[i].connect()) {
                        mConnectionState = STATE_CONNECTING;
                    }
                    return;
                }
            }
        }
        System.out.println("Trying to create a new connection.");
    }

    public void disconnect(String address) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        if(mBluetoothDeviceAddress[0] != null && mBluetoothDeviceAddress[0].equals(address)) {
            mBluetoothGatt[0].disconnect();
            closeGatt(0);
            if(mBluetoothDeviceAddress[1] == null) { mConnectionState = STATE_DISCONNECTED; }
        } else if(mBluetoothDeviceAddress[1] != null && mBluetoothDeviceAddress[1].equals(address)) {
            mBluetoothGatt[1].disconnect();
            closeGatt(1);
            if(mBluetoothDeviceAddress[0] == null) { mConnectionState = STATE_DISCONNECTED; }
        }
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt = null;
        mBluetoothDeviceAddress = null;
    }

    private void closeGatt(int i) {
        if (mBluetoothGatt[i] == null) {
            return;
        }
        mBluetoothDeviceAddress[i] = null;
        mBluetoothGatt[i].close();
        mBluetoothGatt[i] = null;
    }

    public void write(byte[] value, int gattIndex) { //gatIndex = 0 for gatt1, 1 for gatt2 and 2 for tandem gatt


        if(gattIndex == 0) {
            writeRXCharacteristic(value, mBluetoothGatt[0]);
        } else if(gattIndex == 1) {
            writeRXCharacteristic(value, mBluetoothGatt[1]);
        } else {
            for(int i = 0; i < 2; i++) {
                writeRXCharacteristic(value, mBluetoothGatt[i]);
            }
        }
    }

    private void writeRXCharacteristic(byte[] value, BluetoothGatt gatt) {
        try {
            if (gatt != null) {
                BluetoothGattService RxService = gatt.getService(RX_SERVICE_UUID);
                if (RxService == null) {
                    showMessage("Rx service not found!");
                    broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
                    return;
                }
                BluetoothGattCharacteristic RxChar = RxService.getCharacteristic(RX_CHAR_UUID);
                if (RxChar == null) {
                    showMessage("Rx charateristic not found!");
                    broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
                    return;
                }
                boolean result = RxChar.setValue(value);
                Log.d(TAG, "setValue - status=" + result);
                boolean status = gatt.writeCharacteristic(RxChar);

                Log.d(TAG, "write TXchar - status=" + status);
                return;
            }
        } catch (Exception e) {
            Log.d(TAG, "write TXchar - status= failed");
        }
    }

    private void showMessage(String msg) {
        Log.e(TAG, msg);
    }

    public String[] getConnectedAddress() {
        return mBluetoothDeviceAddress;
    }

    public boolean hasConnection() {
        if(mConnectionState > 0) {
            return true;
        }
        return false;
    }

    public boolean isInTandemMode() {
        for(String a : mBluetoothDeviceAddress) {
            if(a == null)
                return false;
        }
        return true;
    }

}
