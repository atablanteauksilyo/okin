package com.customatic.dewertokin.Fragment;

import android.bluetooth.BluetoothGatt;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.customatic.dewertokin.Base.BaseFragment;
import com.customatic.dewertokin.Command.CommandList;
import com.customatic.dewertokin.Interactives.FragmentInteractive;
import com.customatic.dewertokin.Model.DeviceBluetooth;
import com.customatic.dewertokin.R;

import java.util.ArrayList;
import java.util.Collection;

public class Fragment_LIFT extends BaseFragment implements View.OnClickListener ,View.OnTouchListener{

    private ArrayList<BluetoothGatt> bluetoothGatts;
    private View view;
    private ArrayList<DeviceBluetooth> connectedDevices;

    public Fragment_LIFT(FragmentInteractive interactive){
        this.interactive = interactive;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.connectedDevices = new ArrayList<>();
        bluetoothGatts = new ArrayList<>();
        return view = inflater.inflate(R.layout.fragment_lift,null);
    }

    @Override
    public void onStart() {
        super.onStart();
        TextView head_out = this.view.findViewById(R.id.head_out);
        TextView head_in = this.view.findViewById(R.id.head_in);
        TextView leg_in = this.view.findViewById(R.id.leg_in);
        TextView leg_out = this.view.findViewById(R.id.leg_out);
        TextView foot_in = this.view.findViewById(R.id.foot_in);
        TextView foot_out = this.view.findViewById(R.id.foot_out);
        Button setting = this.view.findViewById(R.id.set_button);
        Button light = this.view.findViewById(R.id.light);
        Button anti_snore = this.view.findViewById(R.id.anti_snore);
        Button inline = this.view.findViewById(R.id.inline);
        Button lounge = this.view.findViewById(R.id.lounge);
        Button zg = this.view.findViewById(R.id.zg);

        light.setOnClickListener(this);
        setting.setOnClickListener(this);
       anti_snore.setOnClickListener(this);
       inline.setOnClickListener(this);
       lounge.setOnClickListener(this);
       zg.setOnClickListener(this);
      head_in.setOnTouchListener(this);
      head_out.setOnTouchListener(this);
      leg_in.setOnTouchListener(this);
      leg_out.setOnTouchListener(this);
      foot_in.setOnTouchListener(this);
      foot_out.setOnTouchListener(this);


    }

    @Override
    public void onActivityCallback(Object data) {
        super.onActivityCallback(data);
        if(data instanceof ArrayList){

        }

    }



    private void setAlertClicks(final AlertDialog alertDialog){
        alertDialog.findViewById(R.id.foot_plus).setOnClickListener(this);
        alertDialog.findViewById(R.id.foot_minus).setOnClickListener(this);
        alertDialog.findViewById(R.id.foot2_plus).setOnClickListener(this);
        alertDialog.findViewById(R.id.foot2_minus).setOnClickListener(this);
        alertDialog.findViewById(R.id.head_plus).setOnClickListener(this);
        alertDialog.findViewById(R.id.head2_plus).setOnClickListener(this);
        alertDialog.findViewById(R.id.head2_minus).setOnClickListener(this);
        alertDialog.findViewById(R.id.head_minus).setOnClickListener(this);
        alertDialog.findViewById(R.id.leg_plus).setOnClickListener(this);
        alertDialog.findViewById(R.id.leg2_plus).setOnClickListener(this);
        alertDialog.findViewById(R.id.leg_minus).setOnClickListener(this);
        alertDialog.findViewById(R.id.leg2_minus).setOnClickListener(this);
        alertDialog.findViewById(R.id.button_apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {
    if(view.getId() == R.id.set_button){ setAlertClicks(showAlertDialog("Settings",getActivity(),R.layout.dialog_setting)); }
    else if(view.getId() == R.id.light){ interactive.onFragmentCallback(CommandList.Bluetooth.CBOX_LIGHT); }
    else if(view.getId() == R.id.leg_plus){ interactive.onFragmentCallback(CommandList.Settings.FOOT_INTENSITY_PLUS_CIRCLE);}
    else if(view.getId() == R.id.leg2_plus){ interactive.onFragmentCallback(CommandList.Settings.FOOT_INTENSITY_PLUS_CIRCLE);}
    else if(view.getId() == R.id.foot_plus){ interactive.onFragmentCallback(CommandList.Settings.FOOT_INTENSITY_PLUS_CIRCLE);}
    else if(view.getId() == R.id.foot2_plus){ interactive.onFragmentCallback(CommandList.Settings.FOOT_INTENSITY_PLUS_CIRCLE);}
    else if(view.getId() == R.id.head_plus){ interactive.onFragmentCallback(CommandList.Settings.HEAD_INTENSITY_PLUS_CIRCLE);}
    else if(view.getId() == R.id.head2_plus){ interactive.onFragmentCallback(CommandList.Settings.HEAD_INTENSITY_PLUS_CIRCLE);}
    else if(view.getId() == R.id.leg_minus){ interactive.onFragmentCallback(CommandList.Settings.FOOT_INTENSITY_MINUS);}
    else if(view.getId() == R.id.leg2_minus){ interactive.onFragmentCallback(CommandList.Settings.FOOT_INTENSITY_MINUS);}
    else if(view.getId() == R.id.foot_minus){ interactive.onFragmentCallback(CommandList.Settings.FOOT_INTENSITY_MINUS);}
    else if(view.getId() == R.id.foot2_minus){ interactive.onFragmentCallback(CommandList.Settings.FOOT_INTENSITY_MINUS);}
    else if(view.getId() == R.id.head_minus){ interactive.onFragmentCallback(CommandList.Settings.HEAD_INTENSITY_MINUS);}
    else if(view.getId() == R.id.head2_minus){ interactive.onFragmentCallback(CommandList.Settings.HEAD_INTENSITY_MINUS);}
    else if(view.getId() == R.id.anti_snore){ interactive.onFragmentCallback(CommandList.Settings.ANTI_SNORE);}
    else if(view.getId() == R.id.inline){ interactive.onFragmentCallback(CommandList.Settings.INLINE);}
    else if(view.getId() == R.id.lounge){ interactive.onFragmentCallback(CommandList.Settings.LOUNGE);}
    else if(view.getId() == R.id.zg){ interactive.onFragmentCallback(CommandList.Settings.ZERO_GRAVITY);}





    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if(view.getId() == R.id.head_in){
            interactive.onFragmentCallback(CommandList.Lift.HEAD_IN);
        }
        else if(view.getId() == R.id.head_out){
            interactive.onFragmentCallback(CommandList.Lift.HEAD_OUT);
        }
        else if (view.getId() == R.id.foot_in){
            interactive.onFragmentCallback(CommandList.Lift.FOOT_IN);
        }
        else if (view.getId() == R.id.foot_out){
            interactive.onFragmentCallback(CommandList.Lift.FOOT_OUT);
        }
        else if (view.getId() == R.id.leg_in){
            interactive.onFragmentCallback(CommandList.Lift.LEG_IN);
        }
        else if (view.getId() == R.id.leg_out){
            interactive.onFragmentCallback(CommandList.Lift.LEG_OUT);

        }
        return false;
    }

    @Override
    public boolean onActivityContinuedCallback(Object data, boolean start) {
        bluetoothGatts.clear();
        if(data instanceof  ArrayList){
            bluetoothGatts.addAll((Collection<? extends BluetoothGatt>) data);
        }
        return super.onActivityContinuedCallback(data, start);
    }
}
