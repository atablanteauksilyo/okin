package com.customatic.dewertokin.Fragment;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanResult;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.customatic.dewertokin.Adapter.RecyclerAdapter;
import com.customatic.dewertokin.Base.BaseFragment;
import com.customatic.dewertokin.Interactives.FragmentInteractive;
import com.customatic.dewertokin.Interactives.ItemInteractive;
import com.customatic.dewertokin.Model.DeviceBluetooth;
import com.customatic.dewertokin.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Fragment_LIST extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener , ItemInteractive {
    private SwipeRefreshLayout refreshLayout;
    private View view;
    private RecyclerAdapter recyclerAdapter;
    private ArrayList<DeviceBluetooth> scanResults;
    private boolean loaded = false;
    public Fragment_LIST(FragmentInteractive interactive){
    this.interactive= interactive;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        scanResults = new ArrayList<>();
        view = inflater.inflate(R.layout.fragment_list,null);
        refreshLayout = this.view.findViewById(R.id.refresher);
        refreshLayout.setOnRefreshListener(this);
        return view;
    }
    @Override
    public void onStart() {
        loaded = true;
        refreshLayout.setRefreshing(true);
        super.onStart();
    }

    public boolean isLoaded() {
        return loaded;
    }

    @Override
    public void onActivityCallback(Object data) {
        super.onActivityCallback(data);
        if(data instanceof ArrayList){
            scanResults.clear();
            refreshLayout.setRefreshing(false);
            TextView textView = this.view.findViewById(R.id.data_refresher);
            textView.setVisibility(View.GONE);
            scanResults.addAll((Collection<? extends DeviceBluetooth>) data);
            try{
             getActivity().runOnUiThread(new Runnable() {
               @Override
               public void run() {
                   if(recyclerAdapter != null) {
                       recyclerAdapter.notifyDataSetChanged();
                   }
                   else{
                       RecyclerView recyclerView = view.findViewById(R.id.recycler);
                       recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                       recyclerAdapter = new RecyclerAdapter(Fragment_LIST.this,scanResults,getContext());
                       recyclerView.setAdapter(recyclerAdapter);
                       checkData();

                   }
               }
           });}
            catch (NullPointerException e){
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onActivityContinuedCallback(Object data, boolean start) {

        if(data instanceof BluetoothDevice){
            checkData(((BluetoothDevice) data).getAddress(),start);
        }

        return super.onActivityContinuedCallback(data, start);
    }

    public void checkData(){
        if (!scanResults.isEmpty() && recyclerAdapter != null){
            int from =0;
            for (DeviceBluetooth deviceBluetooth : scanResults){
                if(deviceBluetooth.isConnected()){
                    swapItem(from);
                }
                from++;
            }
        }
    }
    public void checkData(String address,boolean connected){
        if (!scanResults.isEmpty() && recyclerAdapter != null){
            int from =0;
            for (DeviceBluetooth deviceBluetooth : scanResults){
                if(deviceBluetooth.getScanResult().getDevice().getAddress().equals(address)){
                    deviceBluetooth.setConnected(connected);
                    swapItem(from);
                    return;
                }
                from++;
            }
        }
    }
    public void swapItem(final int fromPosition){
        Log.d("onSwap", "swapItem: ");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Collections.swap(scanResults,fromPosition,0);
                recyclerAdapter.notifyItemMoved(fromPosition,0);
                recyclerAdapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public void onRefresh() {
        TextView textView = this.view.findViewById(R.id.data_refresher);
        textView.setVisibility(View.VISIBLE);
        interactive.onFragmentCallback(Fragment_LIST.this);
    }

    @Override
    public void onItemCallback(Object data,boolean connected) {
        if(data instanceof ScanResult){
            if(connected) {
                interactive.onFragmentCallback((ScanResult) data);
            }
            else {
                interactive.onFragmentContinuedCallback(data,connected);
            }
    }
    }
}
