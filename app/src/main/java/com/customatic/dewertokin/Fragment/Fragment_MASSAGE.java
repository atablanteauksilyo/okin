package com.customatic.dewertokin.Fragment;

import android.bluetooth.BluetoothGatt;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.customatic.dewertokin.Base.BaseFragment;
import com.customatic.dewertokin.Command.CommandList;
import com.customatic.dewertokin.Interactives.FragmentInteractive;
import com.customatic.dewertokin.R;

import java.util.ArrayList;
import java.util.Collection;

public class Fragment_MASSAGE extends BaseFragment implements View.OnClickListener{
    private View view;
    private ArrayList<BluetoothGatt> bluetoothGatts;

    public Fragment_MASSAGE(FragmentInteractive interactive){
        this.interactive = interactive;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        bluetoothGatts = new ArrayList<>();
        return view = inflater.inflate(R.layout.fragment_massage,null);
    }

    @Override
    public void onStart() {
        super.onStart();

        TextView wave1 = view.findViewById(R.id.wave1);
        TextView wave2 = view.findViewById(R.id.wave2);
        TextView wave3 = view.findViewById(R.id.wave3);
        TextView wave4 = view.findViewById(R.id.wave4);

        TextView body1 = view.findViewById(R.id.body1);
        TextView body2 = view.findViewById(R.id.body2);
        ImageButton timer = view.findViewById(R.id.timer);

        ToggleButton toggleButton = view.findViewById(R.id.massage_toggle);

        wave1.setOnClickListener(this);
        wave2.setOnClickListener(this);
        wave3.setOnClickListener(this);
        wave4.setOnClickListener(this);

        body1.setOnClickListener(this);
        body2.setOnClickListener(this);

        timer.setOnClickListener(this);
        toggleButton.setOnClickListener(this);

        interactive.onFragmentContinuedCallback("For Refresh",true);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.wave1) {
            interactive.onFragmentCallback(CommandList.Massage.MASSAGE_WAVE1);
        } else if (view.getId() == R.id.wave2) {
            interactive.onFragmentCallback(CommandList.Massage.MASSAGE_WAVE2);
        } else if (view.getId() == R.id.wave3) {
            interactive.onFragmentCallback(CommandList.Massage.MASSAGE_WAVE3);
        } else if (view.getId() == R.id.wave4) {
            interactive.onFragmentCallback(CommandList.Massage.MASSAGE_WAVE4);
        }else if (view.getId() == R.id.body1){
            interactive.onFragmentCallback(CommandList.Massage.BODY1);
        }else  if (view.getId() == R.id.body2){
            interactive.onFragmentCallback(CommandList.Massage.BODY2);
        }else if(view.getId() == R.id.massage_toggle){
            interactive.onFragmentCallback(CommandList.Massage.MASSAGE_ON);
        }else if (view.getId() == R.id.timer){
            setAlertClicks(showAlertDialog("Timer",getActivity(),R.layout.dialog_timer));
        }
    }
    private void setAlertClicks(final AlertDialog alertDialog){
        alertDialog.findViewById(R.id.cancel_timer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.findViewById(R.id.time_10).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                interactive.onFragmentCallback(CommandList.Settings.TIMER_10);
                alertDialog.dismiss();
            }
        });
        alertDialog.findViewById(R.id.time_20).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                interactive.onFragmentCallback(CommandList.Settings.TIMER_20);
                alertDialog.dismiss();
            }
        });
        alertDialog.findViewById(R.id.time_30).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                interactive.onFragmentCallback(CommandList.Settings.TIMER_30);
                alertDialog.dismiss();
            }
        });
    }

    @Override
    public void onActivityCallback(Object data) {
        super.onActivityCallback(data);
        if(data instanceof ArrayList){
            Log.d("TAG", "onActivityCallback: ");
        }
    }

    @Override
    public boolean onActivityContinuedCallback(Object data, boolean start) {
        bluetoothGatts.clear();
        if(data instanceof  ArrayList){
            bluetoothGatts.addAll((Collection<? extends BluetoothGatt>) data);


        }
        return super.onActivityContinuedCallback(data, start);
    }
}
