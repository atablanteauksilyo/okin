package com.customatic.dewertokin.Interactives;

public interface ActivityInteractive<T> {
    void onActivityCallback(T data);
    boolean onActivityContinuedCallback(T data, boolean start);
}
