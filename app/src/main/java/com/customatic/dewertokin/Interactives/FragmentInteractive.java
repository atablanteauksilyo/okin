package com.customatic.dewertokin.Interactives;

public interface FragmentInteractive<T> {

    void onFragmentCallback(T data);
    boolean onFragmentContinuedCallback(T data, boolean start);
}
