package com.customatic.dewertokin.Interactives;

public interface ItemInteractive<T> {
    void onItemCallback(T data,boolean connect);
}
