package com.customatic.dewertokin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.customatic.dewertokin.Activity.FragmentHolderActivity;
import com.customatic.dewertokin.Base.BaseActivity;

public class MainActivity extends BaseActivity {

    @Override
    public void splash_end() {
        Intent intent = new Intent(getApplicationContext(), FragmentHolderActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        splash_start();
    }
}
