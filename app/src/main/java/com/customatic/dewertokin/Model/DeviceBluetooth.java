package com.customatic.dewertokin.Model;

import android.bluetooth.le.ScanResult;

public class DeviceBluetooth {
    private boolean isConnected;
    private ScanResult scanResult;

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    public ScanResult getScanResult() {
        return scanResult;
    }

    public void setScanResult(ScanResult scanResult) {
        this.scanResult = scanResult;
    }

    public DeviceBluetooth(boolean isConnected, ScanResult scanResult) {
        this.isConnected = isConnected;
        this.scanResult = scanResult;
    }
}
